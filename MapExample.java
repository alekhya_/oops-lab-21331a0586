import java.util.*;
public class MapExample {
 public static void main(String[] args) {
 Map<String, Integer> studentScores = new HashMap<>();
 studentScores.put("A", 90);
 studentScores.put("B", 85);
 studentScores.put("C", 95);
 int score1 = studentScores.get("A");
 int score2 = studentScores.get("B");
 System.out.println("A's score: " + score1);
 System.out.println("B's score: " + score2);
 boolean containsKey = studentScores.containsKey("C");
 System.out.println("Does map contain C? " + containsKey);
 studentScores.put("A", 92);
 studentScores.remove("B");
 System.out.println("Keys in the map:");
 for (String key : studentScores.keySet()) {
 System.out.println(key);
 }
 System.out.println("Values in the map:");
 for (int value : studentScores.values()) {
 System.out.println(value);
 }
 System.out.println("Key-value pairs in the map:");
 for (Map.Entry<String, Integer> entry : studentScores.entrySet()) {
 String name = entry.getKey();
 int score = entry.getValue();
 System.out.println(name + ": " + score);
 }
 }
}