#include<iostream>
using namespace std;

// Base class
class Animal{
    public:
        void legs(){
            cout << " I have 4 legs!!! " << endl;
        }
        void tail(){
            cout << " I have a tail!!! " << endl;
        }
};

// Derived class
class Dog : public Animal{
    public:
        void bark(){
            cout << " I am a pet animal, I can bark, woof woof!!! " << endl;
        }
};

int main(){

    //Object for the derived class
    Dog sweety;

    //calling objects of the base class
    sweety.legs();
    sweety.tail();

    //calling objects of the derived class
    sweety.bark();
    return 0;
}

