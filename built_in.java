import java.util.Scanner;

public class built_in {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    // Example 1: ArrayIndexOutOfBoundsException
    int[] numbers = {1, 2, 3};
    try {
      System.out.println(numbers[3]);
    } catch (ArrayIndexOutOfBoundsException e) {
      System.out.println("Exception caught: " + e);
    }

    // Example 2: NullPointerException
    String str = null;
    try {
      System.out.println(str.length());
    } catch (NullPointerException e) {
      System.out.println("Exception caught: " + e);
    }

    // Example 3: ArithmeticException
    System.out.print("Enter a number: ");
    try {
      int num = scanner.nextInt();
      int result = 100 / num;
      System.out.println("Result: " + result);
    } catch (ArithmeticException e) {
      System.out.println("Exception caught: " + e);
    }
  }
}

