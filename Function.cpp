/*Demonstrate method/function overlaoding in C++.*/

#include<iostream>
using namespace std;
class Function{
    public:
    static int add(int a, int b){
        return a+b;
    }
    static int add(int a,int b, int c){
        return a+b+c;
    }
};
int main(){
    cout<<Function::add(3,4)<<endl;
    cout<<Function::add(4,5,6)<<endl;
}