/*Partial abstraction*/

#include<iostream>
using namespace std;

class Shape {  
    public:
    virtual int Answer(int Side) = 0;
    void display(){
        cout<<"The shape is Square"<<endl;
    }
};

class Area : public Shape {
public:
    int Answer(int Side) {
        return Side*4;
    }
};

class Perimeter : public Shape {
public:
    int Answer(int Side) {
        return Side*Side;
    }
};

int main() {
    Area area;
    Perimeter peri;
    int Side;
    cout<<"Enter the dimension of the object"<<endl;
    cin>>Side;
    cout<<"The Area of the object is "<<area.Answer(Side)<<endl;
    cout<<"The Perimeter of the object is "<<peri.Answer(Side)<<endl;

}
