
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;


public class FileChannelExample {
  public static void main(String[] args) throws Exception {
    // Create a RandomAccessFile object for the given file name
    RandomAccessFile file = new RandomAccessFile("example.txt", "rw");


    // Get the FileChannel from the RandomAccessFile object
    FileChannel channel = file.getChannel();


    // Create a ByteBuffer with some sample data
    ByteBuffer buffer = ByteBuffer.allocate(1024);
    buffer.put("Hello World".getBytes());
    buffer.flip();


    // Write the data to the channel
    channel.write(buffer);


    // Reset the buffer for reading
    buffer.clear();


    // Read the data from the channel into the buffer
    channel.read(buffer);


    // Print the data from the buffer
    System.out.println(new String(buffer.array()));


    // Close the channel and file
    channel.close();
    file.close();
  }
}
