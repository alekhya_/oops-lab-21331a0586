/* method/function overlaoding using inheritance */

class Parent{
    public int add(int a, int b){
        return a+b;
    }
}

class Child extends Parent{
    public double add(double a, double b){
        return a+b;
    }
}

class Function{
   
    public static void main(String[] args) {
        Child fun = new Child();
        System.out.println(fun.add(2,6));
        System.out.println(fun.add(5.7,6.6)); 
        System.out.println(fun.add(5,'a')); 
    }
}