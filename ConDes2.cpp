#include <iostream>
using namespace std;
class Student
{
public:
    string fullName, collegeName;
    int collegeCode;
    double semPercentage;
    // Default constructor
    Student()
    {
        collegeName = "MVGR";
        collegeCode = 33;
    }
    // Parameterized constructor
    Student(Student &S)
    {
        cout << "Enter name : ";
        getline(cin, S.fullName);
        cout << "Enter semPercentage : ";
        cin >> S.semPercentage;
    }
    // Destructor
    ~Student()
    {
        cout << "Student object has been destroyed." << endl;
    }

    void display()
    {
        cout << "Full Name : " << fullName << endl;
        cout << "Sem Percentage : " << semPercentage << endl;
        cout << "College Name : " << collegeName << endl;
        cout << "College Code : " << collegeCode << endl;
    }
};

int main()
{
    Student student1;
    Student student2 = student1;
    student1.display();
}
