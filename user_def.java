class CustomException extends Exception {
  public CustomException(String errorMessage) {
    super(errorMessage);
  }
}

public class Throw_exception {
  public static void main(String[] args) throws CustomException {
    int age = 17;
    if (age < 18) {
      throw new CustomException("You must be at least 18 years old.");
    } else {
      System.out.println("You are old enough to vote.");
    }
  }
}



  