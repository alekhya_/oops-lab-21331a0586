/*Write a C++ class 'AccessSpecifierDemo' with the following members:
Member Variables :
1. private int priVar
2. protected int proVar
3. public int pubVar
Member Methods:
1. public void setVar(int priValue,int proValue, int pubValue)
2. public void getVar()
Assign values for each member variable(priVar,proVar,pubvar) and using methods(setVar(),getVar()) and disaply them.*/

#include<iostream>
using namespace std;

class AccessSpecifierDemo {
private:
    int priVar;
protected:
    int proVar;
public:
    int pubVar;
    
    void setVar(int priValue, int proValue, int pubValue) {
        priVar = priValue;
        proVar = proValue;
        pubVar = pubValue;
    }
    
    void getVar() {
        cout << "Private Variable: " << priVar << endl;
        cout << "Protected Variable: " << proVar << endl;
        cout << "Public Variable: " << pubVar << endl;
    }
};

int main() {
    int privar,provar,pubvar;
    cout<<"Enter any three variables"<<endl;
    cin>>privar>>provar>>pubvar;
    AccessSpecifierDemo obj;
    obj.setVar(privar, provar, pubvar);
    obj.getVar();
    return 0;
}










